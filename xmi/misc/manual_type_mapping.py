from typing import Dict


class TypeDetails:
    def __init__(self, name_in_model: str, java_package: str, java_class_name):
        self.name_in_model = name_in_model
        self.java_package = java_package
        self.is_primitive = len(java_package.strip()) == 0
        self.java_class_name = java_class_name


class ManualTypeMapping:
    def __init__(self):
        self.type_mapping: Dict[str, TypeDetails] = dict()
        self.register(name_in_model='T_BOOLEAN', java_package='', java_class_name='boolean')
        self.register(name_in_model='T_FLOAT32', java_package='', java_class_name='double')
        self.register(name_in_model='T_FLOAT64', java_package='', java_class_name='double')
        self.register(name_in_model='T_SINT16', java_package='', java_class_name='int')
        self.register(name_in_model='T_UINT16', java_package='', java_class_name='int')
        self.register(name_in_model='T_SINT32', java_package='', java_class_name='int')
        self.register(name_in_model='T_UINT32', java_package='', java_class_name='int')
        self.register(name_in_model='T_UINT64', java_package='', java_class_name='int')
        self.register(name_in_model='T_STRING', java_package='', java_class_name='String')
        self.register(name_in_model='int', java_package='', java_class_name='int')

        self.register(name_in_model='String', java_package='', java_class_name='String')
        self.register(name_in_model='boolean', java_package='', java_class_name='boolean')
        self.register(name_in_model='Base_SystemIDType', java_package='', java_class_name='String')
        self.register(name_in_model='Base_C2EntityIDType', java_package="java.util", java_class_name='UUID')
        self.register(name_in_model='Base_ExternalSystemIDType', java_package='', java_class_name='String')
        self.register(name_in_model='Base_DateUTCTimeType', java_package='java.time', java_class_name='Instant')
        self.register(name_in_model='dtDataVersion', java_package='', java_class_name='int')
        self.register(name_in_model='dtEntityNumber', java_package='', java_class_name='int')

    def register(self, name_in_model: str, java_package: str, java_class_name):
        self.type_mapping[name_in_model] = TypeDetails(name_in_model=name_in_model, java_package=java_package, java_class_name=java_class_name)

    def get_type_details(self, name_in_model: str) -> TypeDetails:
        return self.type_mapping[name_in_model]

    def is_registered_type(self, model_name: str) -> bool:
        return model_name in self.type_mapping.keys()
