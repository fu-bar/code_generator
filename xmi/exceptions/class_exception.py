from xmi.exceptions.parsing_exception import ParsingException


class ClassException(ParsingException):
    pass
