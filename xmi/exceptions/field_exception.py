from xmi.exceptions.parsing_exception import ParsingException


class FieldException(ParsingException):
    pass
