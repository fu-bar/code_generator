from xmi.exceptions.parsing_exception import ParsingException


class WarningException(ParsingException):
    pass
