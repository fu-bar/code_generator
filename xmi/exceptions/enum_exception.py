from xmi.exceptions.parsing_exception import ParsingException


class EnumException(ParsingException):
    pass
