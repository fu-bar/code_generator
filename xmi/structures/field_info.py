# noinspection PyPackageRequirements
from typing import List

from benedict import benedict

from helpers.utils import ensure_is_list
from xmi.misc.common_strings import xmi_id
from xmi.misc.common_strings import xmi_id_ref
from xmi.misc.common_strings import xmi_name


class FieldInfo:
    def __init__(self, field_element: benedict, element_infos, fields_additional_info):
        self.ea_id = None
        self.name = None
        self.type_ea_id = None
        self.type_name = None
        self.java_doc = None
        self.default_value = None
        self.tags = None
        self.is_collection = None
        self.is_reference = False
        self.validations = dict()
        if len(field_element) == 0:
            return  # To be filled manually
        self.ea_id = field_element[xmi_id]
        self.name = field_element[xmi_name]
        self.type_ea_id = element_infos[field_element['type'][xmi_id_ref]][xmi_id]
        self.type_name = element_infos[field_element['type'][xmi_id_ref]][xmi_name]
        if type(fields_additional_info) == list:
            additional_info = benedict(next((info for info in fields_additional_info if info[xmi_id_ref] == self.ea_id)))
        else:
            additional_info = benedict(fields_additional_info)
        self.java_doc = additional_info['documentation.@value'] if 'documentation.@value' in additional_info else None
        self.default_value = additional_info['initial']
        self.tags = additional_info['tags']
        self.is_collection = additional_info['properties']['@collection'] != 'false'
        if self.tags is not None:
            self.process_tags([benedict(tag) for tag in ensure_is_list(self.tags['tag'])])

    # TODO: Handle additional tags (such as validations)
    def process_tags(self, tags: List):
        for tag in tags:
            tag_name = tag[xmi_name]
            tag_value = tag['@value'] if '@value' in tag else None
            if tag_name == 'isReference' and tag_value != 'false':
                self.is_reference = True
            elif tag_name == 'minValue':
                self.validations['minValue'] = int(tag_value) if '.' not in tag_value else float(tag_value)
            elif tag_name == 'maxValue':
                self.validations['maxValue'] = int(tag_value) if '.' not in tag_value else float(tag_value)
