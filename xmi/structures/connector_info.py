# noinspection PyPackageRequirements
from benedict import benedict

from xmi.misc.common_strings import xmi_id_ref
from xmi.misc.common_strings import xmi_name


class ConnectorInfo:
    def __init__(self, connector_element: benedict):
        self.connector_type = connector_element['properties.@ea_type']  # Generalization / NoteLink / Aggregation
        self.connector_name = connector_element[xmi_name] if xmi_name in connector_element else None
        self.has_name = self.connector_name is not None
        self.connector_ea_id = connector_element[xmi_id_ref]
        self.source_ea_id = connector_element['source.@xmi:idref']
        self.target_ea_id = connector_element['target.@xmi:idref']
        self.java_doc = connector_element['documentation.@value'] if 'documentation' in connector_element and connector_element['documentation'] is not None else None

        self.source_multiplicity = None  # Applicable only to connectors of type Aggregation
        self.target_multiplicity = None  # Applicable only to connectors of type Aggregation
        if self.connector_type == 'Aggregation':
            source_key = 'source.type.@multiplicity'
            target_key = 'target.type.@multiplicity'
            self.source_multiplicity = connector_element[source_key] if source_key in connector_element else None
            self.target_multiplicity = connector_element[target_key] if target_key in connector_element else None
