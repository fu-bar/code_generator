import os
from typing import Dict
from typing import List

import xmltodict
# noinspection PyPackageRequirements
from benedict import benedict

from helpers import log_helper
from helpers.utils import normalize_package_name
from xmi.exceptions.fatal_exception import FatalException
from xmi.exceptions.parsing_exception import ParsingException
from xmi.exceptions.warning_exception import WarningException
from xmi.misc.common_strings import xmi_id
from xmi.misc.common_strings import xmi_id_ref
from xmi.misc.common_strings import xmi_name
from xmi.misc.common_strings import xmi_type_key
from xmi.misc.manual_type_mapping import ManualTypeMapping
from xmi.structures.class_info import ClassInfo
from xmi.structures.connector_info import ConnectorInfo
from xmi.structures.enum_info import EnumInfo
from xmi.structures.package_info import PackageInfo
from xmi.structures.type_details import TypeDetails

# region Constants
xmi_version_key = 'xmi:XMI.@xmi:version'
exporter_name_key = 'xmi:XMI.xmi:Documentation.@exporter'
exporter_version_key = 'xmi:XMI.xmi:Documentation.@exporterVersion'

# Defined by the client in requirements
# The "Folder" in EA under which all the content for generation resides
model_entry_point = 'Class Model'
uml_package_key = 'uml:Package'

# endregion

log = log_helper.get_logger(logger_name='ModelStructure')


class ModelStructure:

    # noinspection SpellCheckingInspection
    def __init__(self, xmi_file_path: str):

        # region Reading model XMI
        if not os.path.isfile(xmi_file_path):
            raise FatalException('File does not exist: {}'.format(xmi_file_path))

        log.debug('Reading XMI from {}'.format(xmi_file_path))
        with open(xmi_file_path, 'r') as xmi_file:
            self.full_model = benedict(xmltodict.parse(xmi_file.read(), process_namespaces=False))
        # endregion

        # region Initializations
        self.class_model = None
        self.element_id_to_element: Dict[str, Dict] = dict()
        self.connector_infos: List[ConnectorInfo] = []
        self.enum_id_to_info: Dict[str, EnumInfo] = dict()
        self.class_id_to_info: Dict[str, ClassInfo] = dict()
        self.package_id_to_info: Dict[str, PackageInfo] = dict()
        self.exceptions: List[ParsingException] = []
        self.manual_mapping = ManualTypeMapping()
        # endregion

        # region Collecting XMI metadata
        missing_keys = [key for key in (xmi_version_key, exporter_name_key, exporter_version_key) if key not in self.full_model]
        if len(missing_keys) != 0:
            raise FatalException('Model XMI is missing the following keys: {}'.format(missing_keys))

        self.xmi_version = self.full_model[xmi_version_key]
        self.exporter = self.full_model[exporter_name_key]
        self.exporter_version = self.full_model[exporter_version_key]
        log.debug('XMI info: xmi-version={}, exporter={}, exporter-version={}'.format(self.xmi_version, self.exporter, self.exporter_version))
        # endregion

        # region Actual XMI processing procedure
        self.detect_class_model_entry_point()
        self.process_package_hierarchy()
        self.collect_elements()
        self.collect_connectors()
        self.process_enums()
        self.process_classes()
        # endregion

    # region Content pre-processing
    def detect_class_model_entry_point(self):
        class_model_candidates = self.full_model.search(model_entry_point,
                                                        in_keys=False,
                                                        in_values=True,
                                                        exact=True,
                                                        case_sensitive=False)
        # Not an extension (@xmi:idref), the name matches and the type is uml:Package
        class_model = [
                candidate for candidate in class_model_candidates if
                xmi_id_ref not in candidate[0] and
                xmi_name in candidate[0] and
                xmi_type_key in candidate[0] and
                candidate[0][xmi_name] == model_entry_point and
                candidate[0][xmi_type_key] == 'uml:Package'
        ]

        if len(class_model) != 1:
            error_message = 'Expected exactly 1 candidate for class_model but found {}'.format(len(class_model))
            log.error(error_message)
            raise FatalException(error_message)

        self.class_model = class_model[0]

    def process_package_hierarchy(self):
        log.debug('Processing packages hierarchy')
        extensions_xmi_section = self.full_model['xmi:XMI.xmi:Extension']

        uml_packages = extensions_xmi_section.search(query=uml_package_key,
                                                     in_keys=False,
                                                     in_values=True,
                                                     exact=True,
                                                     case_sensitive=True)
        for uml_package in uml_packages:
            package_dict = uml_package[0]
            package_info = PackageInfo(package_element=benedict(package_dict))
            if package_info.ea_id in self.package_id_to_info:
                raise FatalException('Package {} encountered more than once. (ea-id {})'.format(package_info.name, package_info.ea_id))
            self.package_id_to_info[package_info.ea_id] = package_info

        log.debug('{} UML packages detected: {}'.format(len(self.package_id_to_info.keys()), [p.name for p in self.package_id_to_info.values()]))

    def collect_elements(self):
        element_id_index = 2
        element_index = 0
        log.debug('Collecting all elements having an xmi-id')
        search_results = self.full_model.search(query=xmi_id,
                                                in_keys=True,
                                                in_values=False,
                                                exact=True,
                                                case_sensitive=True)
        for result in search_results:
            element_id = result[element_id_index]
            the_element = result[element_index]
            self.element_id_to_element[element_id] = benedict(the_element)

    def collect_connectors(self):
        log.debug('Processing connectors')
        connectors = self.full_model['xmi:XMI.xmi:Extension.connectors.connector']
        for connector_element in connectors:
            self.connector_infos.append(ConnectorInfo(connector_element=benedict(connector_element)))

    # endregion

    def process_enums(self):
        uml_enumeration_key = 'uml:Enumeration'
        log.debug('Processing enumerations')
        enumerations_in_model = [benedict(item) for item in self.full_model['xmi:XMI.xmi:Extension.elements.element'] if xmi_type_key in item and item[xmi_type_key] == uml_enumeration_key]
        for enumeration_xmi_element in enumerations_in_model:
            self.set_java_packages(into_item=enumeration_xmi_element)
            try:
                enum_info = EnumInfo(enumeration_xmi_element=enumeration_xmi_element,
                                     element_id_to_element=self.element_id_to_element)
                self.enum_id_to_info[enum_info.ea_id] = enum_info
            except ParsingException as exception:
                log.warning(str(exception))
                self.exceptions.append(exception)
                continue

        # ENUM Inheritance is not equivalent to OOP inheritance.
        # Inheriting ENUM has inherited ordinals with values, but no actual hierarchical relation to its "ancestor"
        for enum_info in self.enum_id_to_info.values():
            if enum_info.inherits_from_ea_id is not None:
                if enum_info.inherits_from_ea_id not in self.enum_id_to_info:
                    raise FatalException('ENUM {} supposed to inherit from a non existing element with id: {}'.format(enum_info.name, enum_info.inherits_from_ea_id))
                ancestor_enum = self.enum_id_to_info[enum_info.inherits_from_ea_id]
                log.debug('ENUM {} inherits from {}'.format(enum_info.name, ancestor_enum.name))
                enum_info.ea_id_to_value_name = {**enum_info.ea_id_to_value_name, **ancestor_enum.ea_id_to_value_name}
                enum_info.ea_id_to_value = {**enum_info.ea_id_to_value, **ancestor_enum.ea_id_to_value}

        log.info('Collected {} enumerations : {}'.format(len(self.enum_id_to_info.keys()), [enum.name for enum in self.enum_id_to_info.values()]))

    def process_classes(self):
        uml_class_key = 'uml:Class'
        log.debug('Processing classes')
        class_model_content = benedict(self.class_model[0])

        class_additional_info = [element for element in self.full_model['xmi:XMI.xmi:Extension.elements.element'] if xmi_type_key in element and element[xmi_type_key] == uml_class_key]
        class_search_results = class_model_content.search(uml_class_key,
                                                          in_keys=False,
                                                          in_values=True,
                                                          exact=True,
                                                          case_sensitive=False)
        for class_result in class_search_results:
            class_result_content = class_result[0]
            try:
                element_ea_id = class_result_content[xmi_id]
                element_additional_info = [item for item in class_additional_info if item[xmi_id_ref] == element_ea_id]
                if len(element_additional_info) != 1:
                    raise WarningException('[SKIPPING (not a class?)] Skipping {} because it does not have a single additional info ({})'.format(class_result_content[xmi_name], element_ea_id))

                element_additional_info = element_additional_info[0]
                self.set_java_packages(into_item=benedict(element_additional_info))
                class_package_name = self.package_id_to_info[benedict(element_additional_info)['model.@package']].name
                if class_package_name == 'Element Types':
                    log.debug('Not generating a class for {} because it is under {}'.format(benedict(element_additional_info)[xmi_name], class_package_name))
                    continue  # Not to be generated (these `classes` are String, int...)
                class_info = ClassInfo(
                        class_element=benedict(class_result_content),
                        class_additional_info=benedict(element_additional_info),
                        element_infos=self.element_id_to_element,
                        connector_infos=self.connector_infos
                )
                self.class_id_to_info[class_info.ea_id] = class_info
            except ParsingException as parsing_exception:
                log.warning(str(parsing_exception))
                self.exceptions.append(parsing_exception)
                continue

        for info in self.class_id_to_info.values():
            info.collect_fields_based_on_aggregation_links(class_infos=self.class_id_to_info)
        log.info('Collected {} classes: {}'.format(len(self.class_id_to_info.keys()), [class_info.name for class_info in self.class_id_to_info.values()]))

    # region Utility methods
    def set_java_packages(self, into_item: Dict):
        java_package = ''
        item_package_ea_id = into_item['model.@package']
        package_name = self.package_id_to_info[item_package_ea_id].name
        while package_name != 'Class Model':
            java_package = package_name if len(java_package) == 0 else '{}.{}'.format(package_name, java_package)
            item_package_ea_id = self.package_id_to_info[item_package_ea_id].ancestor_ea_id
            package_name = self.package_id_to_info[item_package_ea_id].name
        java_package = normalize_package_name(java_package)
        into_item['@java_package'] = java_package

    def get_import_details(self, ea_id) -> TypeDetails:
        is_class = ea_id in self.class_id_to_info
        is_enumeration = ea_id in self.enum_id_to_info
        type_details = TypeDetails()
        if is_class is False and is_enumeration is False:
            type_details.import_java_package = None
            element_name = self.element_id_to_element[ea_id][xmi_name]
            if not self.manual_mapping.is_registered_type(element_name):
                raise ParsingException('[MANUAL TRANSLATION] Missing manual translation value for type: {}'.format(element_name))
            details = self.manual_mapping.get_type_details(name_in_model=element_name)
            type_details.class_name = details.java_class_name
            if not details.is_primitive:
                type_details.import_java_package = details.java_package
        else:
            element = self.class_id_to_info[ea_id] if is_class else self.enum_id_to_info[ea_id]
            type_details.class_name = element.name
            type_details.import_java_package = element.java_package
        return type_details
    # endregion
