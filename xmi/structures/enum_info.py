# noinspection PyPackageRequirements
from typing import Dict

# noinspection PyPackageRequirements
from benedict import benedict

from helpers.utils import ensure_is_list
from xmi.exceptions.enum_exception import EnumException
from xmi.misc.common_strings import xmi_id
from xmi.misc.common_strings import xmi_id_ref
from xmi.misc.common_strings import xmi_name

java_doc_key = 'properties.@documentation'
enum_details_key = 'attributes.attribute'
enum_generalization_key = 'Generalization'
links_key = 'links'


class EnumInfo:
    def __init__(self, enumeration_xmi_element: benedict, element_id_to_element: Dict[str, Dict]):
        # region Initialization
        self.ea_id = enumeration_xmi_element[xmi_id_ref]
        self.name = enumeration_xmi_element[xmi_name]
        self.ea_id_to_value_name = dict()
        self.ea_id_to_value = dict()
        self.java_package = enumeration_xmi_element['@java_package']

        self.java_doc = enumeration_xmi_element[java_doc_key] if java_doc_key in enumeration_xmi_element else None
        self.inherits_from_ea_id = None
        # endregion

        if enum_details_key not in enumeration_xmi_element:
            raise EnumException('[NO-ENUM-FIELDS] No value fields found for {} under {} ({})'.format(self.name, self.java_package, self.ea_id))

        for element in ensure_is_list(enumeration_xmi_element[enum_details_key]):
            self.ea_id_to_value_name[element[xmi_id_ref]] = element[xmi_name]
            self.ea_id_to_value[element[xmi_id_ref]] = int(element['initial']['@body'])

        # Detecting "enum-inheritance"
        if links_key in enumeration_xmi_element and enum_generalization_key in enumeration_xmi_element[links_key]:
            link_end = enumeration_xmi_element['links.Generalization.@end']
            is_outgoing_link = element_id_to_element[link_end][xmi_id] != enumeration_xmi_element[xmi_id_ref]
            if is_outgoing_link:
                self.inherits_from_ea_id = element_id_to_element[link_end][xmi_id]
