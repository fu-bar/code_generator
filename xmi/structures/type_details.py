from typing import Union


class TypeDetails:
    def __init__(self):
        self.import_java_package = None
        self.class_name = None

    def get_full_name(self) -> Union[str, None]:
        if self.import_java_package is None or self.class_name is None:
            return None
        else:
            return '{}.{}'.format(self.import_java_package, self.class_name)
