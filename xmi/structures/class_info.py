from typing import Dict
from typing import List

# noinspection PyPackageRequirements
from benedict import benedict

from helpers import log_helper
from helpers.utils import ensure_is_list
from xmi.exceptions.class_exception import ClassException
from xmi.misc.common_strings import xmi_documentation
from xmi.misc.common_strings import xmi_generalization
from xmi.misc.common_strings import xmi_id
from xmi.misc.common_strings import xmi_name
from xmi.misc.common_strings import xmi_stereotype
from xmi.structures.connector_info import ConnectorInfo
from xmi.structures.field_info import FieldInfo

entity_stereotype = 'C2Entity'
message_stereotype = 'C2Message'

log = log_helper.get_logger('ClassInfo')


class ClassInfo:
    def __init__(self, class_element: benedict, class_additional_info: benedict, element_infos, connector_infos: List[ConnectorInfo]):
        # region Initializations
        self.ea_id = class_element[xmi_id]
        self.name = class_element[xmi_name]
        self.package_ea_id = class_additional_info['model.@package']
        self.visibility = class_element['@visibility']  # Will probably not use it
        self.properties = class_additional_info['properties']
        self.is_entity = xmi_stereotype in self.properties and self.properties[xmi_stereotype] == entity_stereotype
        self.is_message = xmi_stereotype in self.properties and self.properties[xmi_stereotype] == message_stereotype
        self.java_doc = self.properties[xmi_documentation] if xmi_documentation in self.properties else None
        self.field_infos: List[FieldInfo] = []
        self.java_package = class_additional_info['@java_package']
        self.inherits_from = None
        # endregion

        self.set_inheritance(class_element, element_infos)

        self.aggregation_connections: Dict = dict()

        fields_key = 'ownedAttribute'
        if fields_key not in class_element:
            message = '[USELESS CLASS] Class {} owns no fields (ea-id: {})'.format(self.name, self.ea_id)
            log.warning(message)
            class_element[fields_key] = []
            # raise ClassException(message)

        fields_additional_info_key = 'attributes.attribute'
        if fields_additional_info_key not in class_additional_info:
            # message = '[NO FIELDS INFORMATION] Class {} has no information about its fields (ea-id: {})'.format(self.name, self.ea_id)
            # log.warning(message)
            class_additional_info[fields_additional_info_key] = []
            # raise ClassException(message)

        field_elements = ensure_is_list(class_element[fields_key])
        fields_additional_info = class_additional_info[fields_additional_info_key]

        for element in field_elements:
            if xmi_name in element:
                self.field_infos.append(
                        FieldInfo(field_element=element,
                                  element_infos=element_infos,
                                  fields_additional_info=fields_additional_info)
                )

        all_class_connectors = [connector for connector in connector_infos if self.ea_id == connector.source_ea_id or self.ea_id == connector.target_ea_id]
        self.collect_aggregation_links(all_class_connectors, element_infos)

    def collect_aggregation_links(self, all_class_connectors, element_infos):
        aggregation_class_connectors = [connector for connector in all_class_connectors if connector.connector_type == 'Aggregation']
        for aggregation_connector in aggregation_class_connectors:
            is_collection = False
            if aggregation_connector.source_multiplicity is not None:
                is_collection = '*' in aggregation_connector.source_multiplicity
            if is_collection:
                collection_type_element = element_infos[aggregation_connector.source_ea_id]
                collection_type_element.collection_java_doc = aggregation_connector.java_doc
                self.aggregation_connections[aggregation_connector.connector_name] = collection_type_element

    def set_inheritance(self, class_element, element_infos):
        if xmi_generalization not in class_element:
            self.inherits_from = None
        else:
            if type(class_element[xmi_generalization]) == list:
                ancestor_ea_ids = [element['@general'] for element in class_element[xmi_generalization]]
                raise ClassException('[MULTIPLE INHERITANCE] Class {} ({}) inherits from multiple elements: {}'.format(
                        self.name,
                        self.ea_id,
                        ancestor_ea_ids)
                )

            ancestor_ea_id = class_element[xmi_generalization]['@general']
            if ancestor_ea_id not in element_infos:
                raise ClassException('[INHERITANCE] Class {} ({}) inherits from ea_id ({}) which is not found'.format(
                        self.name,
                        self.ea_id,
                        ancestor_ea_id)
                )
        if xmi_generalization in class_element:
            self.inherits_from = element_infos[class_element[xmi_generalization]['@general']]

    def collect_fields_based_on_aggregation_links(self, class_infos: Dict):
        if len(self.aggregation_connections) == 0:
            return
        for field_name, connected_element in self.aggregation_connections.items():
            connected_class_info = class_infos[connected_element[xmi_id]]
            manually_filled = benedict()
            field_info = FieldInfo(manually_filled, None, None)
            field_info.name = field_name
            field_info.is_collection = True
            field_info.type_ea_id = connected_class_info.ea_id
            field_info.type_name = connected_class_info.name
            field_info.default_value = None
            field_info.java_doc = connected_element.collection_java_doc
            field_info.tags = None

            self.field_infos.append(field_info)
