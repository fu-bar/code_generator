# noinspection PyPackageRequirements
from benedict import benedict

from xmi.misc.common_strings import xmi_id
from xmi.misc.common_strings import xmi_id_ref
from xmi.misc.common_strings import xmi_name


class PackageInfo:
    def __init__(self, package_element: benedict):
        self.name = package_element[xmi_name]
        self.ea_id = package_element[xmi_id_ref] if xmi_id_ref in package_element else package_element[xmi_id]
        self.ancestor_ea_id = package_element['model.@package'] if 'model' in package_element else None
        if self.name == 'Class Model':
            self.ancestor_ea_id = None
