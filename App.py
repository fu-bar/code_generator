import sys

from helpers import log_helper
from outputs import summary_generator
from xmi.exceptions.fatal_exception import FatalException
from xmi.structures.model_structure import ModelStructure

xmi_file_path = r'c:\workspace\code_generation\xmi\unclassified_model.xml'
output_file_path = r'c:\workspace\code_generation\intermediate\unclassified_intermediate.json'
# xmi_file_path = r'c:\workspace\code_generation\xmi\gadgets.xml'
# output_file_path = r'c:\workspace\code_generation\intermediate\intermediate.json'

if __name__ == '__main__':
    log = log_helper.get_logger('CodeGenerator')
    try:
        model_structure = ModelStructure(xmi_file_path=xmi_file_path)
    except FatalException as fatal_exception:
        log.error('[FATAL-EXCEPTION] {}'.format(str(fatal_exception)))
        sys.exit(1)
    summary = summary_generator.generate_summary(model_structure)

    with open(output_file_path, 'w') as output:
        output.write(summary.dump())
    log.info('Done')
