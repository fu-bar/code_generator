# noinspection PyPackageRequirements
import time

# noinspection PyPackageRequirements
from benedict import benedict

from helpers import log_helper
from xmi.exceptions.class_exception import ClassException
from xmi.exceptions.enum_exception import EnumException
from xmi.exceptions.field_exception import FieldException
from xmi.exceptions.parsing_exception import ParsingException
from xmi.structures.model_structure import ModelStructure


def generate_summary(model_structure: ModelStructure) -> benedict:
    log = log_helper.get_logger(logger_name='SummaryGenerator')
    summary = benedict()

    # region Metadata
    summary['metadata.timestamp'] = int(time.time())
    summary['metadata.xmi_version'] = model_structure.xmi_version
    summary['metadata.exporter'] = model_structure.exporter
    summary['metadata.exporter_version'] = model_structure.exporter_version
    # endregion

    # region Errors
    summary['errors'] = benedict()
    summary['errors.count'] = len(model_structure.exceptions)
    summary['errors.messages'] = [str(exception) for exception in model_structure.exceptions]
    summary['errors.type.class'] = [str(exception) for exception in model_structure.exceptions if type(exception) == ClassException]
    summary['errors.type.enum'] = [str(exception) for exception in model_structure.exceptions if type(exception) == EnumException]
    summary['errors.type.field'] = [str(exception) for exception in model_structure.exceptions if type(exception) == FieldException]
    # endregion

    # region Enumerations
    summary['enumerations'] = benedict()
    enum_section = summary['enumerations']
    for enumeration in model_structure.enum_id_to_info.values():
        enum_section[enumeration.name] = benedict()
        enum_section[enumeration.name]['name'] = enumeration.name
        enum_section[enumeration.name]['items'] = benedict()
        for key, value in enumeration.ea_id_to_value_name.items():
            enum_section[enumeration.name]['items'][value] = enumeration.ea_id_to_value[key]
        enum_section[enumeration.name]['java_package'] = enumeration.java_package
    # endregion

    # region Classes
    summary['classes'] = benedict()
    for class_info in model_structure.class_id_to_info.values():
        package_no_dots = class_info.java_package.replace('.', '::')
        try:
            summary['classes.{}.{}'.format(package_no_dots, class_info.name)] = benedict()
            class_entry = summary['classes.{}.{}'.format(package_no_dots, class_info.name)]
            class_entry['name'] = class_info.name
            class_entry['java_doc'] = class_info.java_doc
            class_entry['java_package'] = class_info.java_package
            class_entry['is_entity'] = class_info.is_entity
            class_entry['is_message'] = class_info.is_message
            class_entry['fields'] = benedict()
            import_requirements = set()
            for field_info in class_info.field_infos:
                class_entry['fields'][field_info.name] = benedict()
                class_entry['fields'][field_info.name]['name'] = field_info.name
                class_entry['fields'][field_info.name]['java_doc'] = field_info.java_doc
                class_entry['fields'][field_info.name]['is_collection'] = field_info.is_collection
                class_entry['fields'][field_info.name]['is_reference'] = field_info.is_reference
                class_entry['fields'][field_info.name]['validations'] = field_info.validations
                type_details = model_structure.get_import_details(ea_id=field_info.type_ea_id)
                if type_details.import_java_package is not None:
                    import_requirements.add(type_details.get_full_name())
                class_entry['fields'][field_info.name]['field_type'] = type_details.class_name
            class_entry['required_imports'] = import_requirements
            inheritance = model_structure.get_import_details(ea_id=class_info.inherits_from['@xmi:id']) if class_info.inherits_from is not None else None
            class_entry['inheritance'] = benedict()
            if inheritance is not None:
                class_entry['inheritance.class_name'] = inheritance.class_name
                class_entry['required_imports'].add(inheritance.get_full_name())
        except ParsingException as parsing_exception:
            summary.remove('classes.{}.{}'.format(package_no_dots, class_info.name))
            log.warning('Removed class: {} ({})'.format(class_info.name, str(parsing_exception)))
            summary['errors.type.class'].append(str(parsing_exception))
            continue

    # endregion

    return summary
