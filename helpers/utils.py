from typing import List


def normalize_package_name(string: str) -> str:
    return string.lower().strip().replace(' ', '_')


def ensure_is_list(element) -> List:
    return element if type(element) == list else [element]
